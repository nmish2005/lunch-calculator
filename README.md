# Lunch Calculator (lunch-calculator)

Web-app that analyzes enjoyment of different food and counts the number of lunches order to; powered by Brain.js & Docker.

## Install the dependencies
```bash
cd quasar
yarn
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```

### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://v2.quasar.dev/quasar-cli/quasar-conf-js).
