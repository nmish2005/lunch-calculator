FROM node:16-buster

WORKDIR /app/back

RUN apt-get update && \
    apt-get install -y build-essential libxi-dev libglu1-mesa-dev libglew-dev pkg-config && \
    yarn global add pm2 nodemon

ENTRYPOINT export PATH="$(yarn global bin):$PATH" && \
           yarn && \
           pm2 start server.js --watch && \
           pm2 logs server.js
