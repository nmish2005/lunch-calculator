const
  iFrame = document.createElement('iframe')

iFrame.id = 'bex-app-iframe'

Object.assign(iFrame.style, {
  border: 0,
  overflow: 'visible',
  width: '100%',
  height: '880px',
  left: 0,
  top: 0
})

const API_HOST = 'https://lunchcalc.duckdns.org'

export default function attachContentHooks(bridge) {
  let y = 0

  function parseDate(str) {
    str = str.split(/\//g).map(i => parseInt(i))
    str[1]--
    return (+new Date(...str) - (new Date).getTimezoneOffset() * 60 * 1000)
  }

  function getNetworks(event) {
    fetch(API_HOST + '/getNetworks')
      .then(r => r.json())
      .then(networks => {
        bridge.send(event.eventResponseKey, networks)
      })
  }

  function isNetAvailable(event) {
    fetch(API_HOST + '/isAvailable')
      .then(r =>
        bridge.send(event.eventResponseKey, r.status !== 422)
      )
      .catch(() => {
      })
  }

  function fetchData(event) {
    console.log('fetching data')

    fetch(API_HOST + '/day/' + (event.data.timestamp || parseDate(event.data.date)))
      .then(r => r.json())
      .then(r =>
        bridge.send(event.eventResponseKey, r)
      )
      .catch(() => {
        bridge.send(event.eventResponseKey, null)
      })
  }

  function insertData(event) {
    console.log('inserting data', event)

    fetch(API_HOST + '/day/' + parseDate(event.data.date), {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        peopleCnt: parseInt(event.data.historicData.peopleCnt),
        lunchCnt: parseInt(event.data.historicData.lunchCnt),
        dinnerCnt: parseInt(event.data.historicData.dinnerCnt),
        lunchesLeft: parseInt(event.data.historicData.lunchesLeft),
        dinnersLeft: parseInt(event.data.historicData.dinnersLeft)
      })
    })
      .then(() =>
        bridge.send(event.eventResponseKey, 'done')
      )
  }

  function openPopup(event) {
    Object.assign(iFrame.style, {
      zIndex: 999,
      height: '100%',
      position: 'absolute'
    })

    y = window.scrollY
    window.scrollTo(0, 0)

    bridge.send(event.eventResponseKey, 'done')
  }

  function closePopup(event) {
    Object.assign(iFrame.style, {
      zIndex: 1,
      height: '880px',
      position: 'static'
    })

    window.scrollTo(0, y)

    bridge.send(event.eventResponseKey, 'done')
  }

  function destroyBridge(event) {
    bridge.off('net.getNetworks', getNetworks)

    bridge.off('net.isAvailable', isNetAvailable)

    bridge.off('net.fetchData', fetchData)

    bridge.off('net.insertData', insertData)

    bridge.off('popup.open', openPopup)

    bridge.off('popup.close', closePopup)

    bridge.off('bridge.destroy', destroyBridge)

    bridge.off('bridge.reload', reloadBridge)

    bridge.send(event.eventResponseKey, 'done')
  }

  function reloadBridge(event) {
    bridge.off('net.getNetworks', getNetworks)

    bridge.off('net.isAvailable', isNetAvailable)

    bridge.off('net.fetchData', fetchData)

    bridge.off('net.insertData', insertData)

    bridge.off('popup.open', openPopup)

    bridge.off('popup.close', closePopup)

    bridge.off('bridge.destroy', destroyBridge)

    bridge.off('bridge.reload', reloadBridge)

    bridge.on('net.getNetworks', getNetworks)

    bridge.on('net.isAvailable', isNetAvailable)

    bridge.on('net.fetchData', fetchData)

    bridge.on('net.insertData', insertData)

    bridge.on('popup.open', openPopup)

    bridge.on('popup.close', closePopup)

    bridge.on('bridge.destroy', destroyBridge)

    bridge.on('bridge.reload', reloadBridge)

    bridge.send(event.eventResponseKey, 'done')
  }

  bridge.on('net.getNetworks', getNetworks)

  bridge.on('net.isAvailable', isNetAvailable)

  bridge.on('net.fetchData', fetchData)

  bridge.on('net.insertData', insertData)

  bridge.on('popup.open', openPopup)

  bridge.on('popup.close', closePopup)

  bridge.on('bridge.destroy', destroyBridge)

  bridge.on('bridge.reload', reloadBridge);
}

;
(function () {
  // When the page loads, insert our browser extension app.
  iFrame.src = chrome.runtime.getURL(`www/index.html`)

  const hostname = "billtech.hurma.work",
    pathname = '/'
  // const hostname = "localhost",
  //   pathname = '/'

  if (window.location.hostname === hostname &&
    window.location.pathname === pathname) {
    document.getElementsByClassName('container-fluid')[0].append(iFrame)
  }
})()
