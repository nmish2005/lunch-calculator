const days = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat']

class DateHelper {
    static today () {
        return DateHelper.getDayId(+new Date())
    }

    static getDayId (timestamp) {
        let startDate = new Date(2021, 0, 2),
            date = new Date(timestamp)

        startDate -= startDate.getTimezoneOffset() * 60 * 1000

        date.setHours(0, 0, 0, 0)

        const weekDiff = Math.floor((date - startDate) / 604800000)

        let index = 1

        if (weekDiff % 2 === 1) {
            index = 2
        }

        return days[date.getDay()] + index
    }
}

module.exports = DateHelper
module.exports.days = days