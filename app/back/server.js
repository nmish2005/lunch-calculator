const express = require('express'),
    mongoose = require('mongoose'),
    https = require('https'),
    fs = require('fs'),
    path = require('path'),
    cors = require('cors'),
    bodyParser = require('body-parser'),
    app = express(),
    DateHelper = require("./DateHelper"),
    NNProcessor = require("./NNProcessor"),
    {days} = require("./DateHelper"),
    credentials = {
        key: fs.readFileSync('/certs/privkey1.pem', 'utf8'),
        cert: fs.readFileSync('/certs/cert1.pem', 'utf8'),
        ca: fs.readFileSync('/certs/chain1.pem', 'utf8')
    };

app.use(bodyParser.json())
app.use(cors())

mongoose.connect('mongodb://mongo:27017/lunchdb', {useNewUrlParser: true, useUnifiedTopology: true})

const OrderHistorySchema = new mongoose.Schema({
    mealIndex: String,
    data: {
        type: Object,
        default: {}
    }
}, { minimize: false })

const OrderHistory = mongoose.model('orderHistory', OrderHistorySchema, 'orderHistory')

function isNumberCorrect(num) {
    const parsed = parseInt(num)

    return !(isNaN(parsed) || parsed <= 0);
}

for (let i = 1; i <= 2; i++) {
    days.filter(i => !!i).forEach(day => {
        OrderHistory.countDocuments({mealIndex: `${day}${i}_lunch`}, (err, count) => {
            if (count < 1) {
                OrderHistory.create({mealIndex: `${day}${i}_lunch`})
            }
        })

        OrderHistory.countDocuments({mealIndex: `${day}${i}_dinner`}, (err, count) => {
            if (count < 1) {
                OrderHistory.create({mealIndex: `${day}${i}_dinner`})
            }
        })
    })
}

app.get('/getNetworks', async (req, res) => {
    const today = DateHelper.today(),
    // const today = 'tue1',
        mappingFn = i => {
            return (i.mealCnt - i.mealsLeft) * (100 / i.peopleCnt) / 100
        },
        lunchData = Object.values((await OrderHistory.find({mealIndex: today + '_lunch'}))[0].data).map(mappingFn),
        dinnerData = Object.values((await OrderHistory.find({mealIndex: today + '_dinner'}))[0].data).map(mappingFn),
        lunchNet = new NNProcessor(lunchData),
        dinnerNet = new NNProcessor(dinnerData)

    console.log()

    res.send({lunchNet: lunchNet.json, dinnerNet: dinnerNet.json})
})

app.get('/isAvailable', async (req, res) => {
    const day = DateHelper.today(),
        lunchData = (await OrderHistory.findOne({mealIndex: day + '_lunch'})).data,
        dinnerData = (await OrderHistory.findOne({mealIndex: day + '_dinner'})).data

    if (Object.keys(lunchData).length === 0 || Object.keys(dinnerData).length === 0) {
        res.status(422).send('no')
    } else {
        res.send('yes')
    }
})

app.get('/day/:timestamp', async (req, res) => {
    if (!isNumberCorrect(req.params.timestamp)) {
        return res.status(422).send('422 Unprocessable entity')
    }

    let timestamp = new Date(parseInt(req.params.timestamp))

    timestamp.setHours(0, 0, 0, 0)
    timestamp = +timestamp

    const day = DateHelper.getDayId(timestamp),
        lunchData = (await OrderHistory.findOne({mealIndex: day + '_lunch'})).data,
        dinnerData = (await OrderHistory.findOne({mealIndex: day + '_dinner'})).data

    if (!!lunchData && !!dinnerData) {
        if (lunchData['date' + timestamp] && dinnerData['date' + timestamp]) {
            return res.send({
                lunch: lunchData['date' + timestamp],
                dinner: dinnerData['date' + timestamp]
            })
        }
    }

    res.status(422).send('422 Unprocessable entity')
})

app.all('/ping', (req, res) => {
    res.send('pong')
})

app.post('/day/:timestamp', async (req, res) => {
    let timestamp = new Date(parseInt(req.params.timestamp))

    timestamp.setHours(0, 0, 0, 0)
    timestamp = +timestamp

    const day = DateHelper.getDayId(timestamp),
        {peopleCnt, lunchCnt, dinnerCnt, dinnersLeft, lunchesLeft} = req.body

    OrderHistory.findOne({mealIndex: day + '_lunch'}, (err, doc) => {
        if (err) {
            throw err
        }

        if (!doc.data) {
            doc.data = {}
        }

        //TODO save predictions too
        doc.data['date' + timestamp] = {
            peopleCnt, mealCnt: lunchCnt, mealsLeft: lunchesLeft
        }

        doc.markModified('data.date' + timestamp)

        doc.save(err1 => {
            if (err1) {
                throw err1
            }

            OrderHistory.findOne({mealIndex: day + '_dinner'}, (err, doc) => {
                if (err) {
                    throw err
                }

                doc.markModified('data.date' + timestamp)

                doc.data['date' + timestamp] = {
                    peopleCnt, mealCnt: dinnerCnt, mealsLeft: dinnersLeft
                }

                doc.save(err2 => {
                    if (err2) {
                        throw err2
                    }

                    res.status(201).send('ok')
                })
            })
        })
    })
})

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, './fake.html'))
})

const server = https.createServer(credentials, app)

server.listen(443, () => {
    console.log(`Example app listening at https://lunchcalc.duckdns.org`)
})
