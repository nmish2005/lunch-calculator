const brain = require('brain.js')

class NNProcessor {
    json = ''
    net = undefined

    constructor(rawTrainingData) {
        const config = {
            hiddenLayers: [3],
            iterations: 50000
        }

        let net, trainingResult, trainingData = this.prepareData(rawTrainingData)

        for (let i = 0; i < 10; i++) {
            net = new brain.NeuralNetwork(config)
            trainingResult = net.train(trainingData);

            if (trainingResult.error < 0.05) {
                break
            }
        }

        console.log('Training error:', trainingResult.error)

        this.net = net
        this.json = net.toJSON()
    }

    prepareData(rawData) {
        let rawTrainingData = [],
            trainingData = []

        for (let i = 0; i < 1; i += 0.1) {
            i = Math.round((i + Number.EPSILON) * 100) / 100

            if (rawData.includes(i)) {
                rawTrainingData.push([[i], [1]])

                rawData = rawData.filter(indexInArray => {
                    return indexInArray !== i
                })
            } else {
                if (rawData.every(item => i - 0.03 > item || i + 0.03 < item)) {
                    rawTrainingData.push([[i], [0]])
                }
            }
        }

        rawData.forEach(i => {
            rawTrainingData.push([[i], [1]])
        })

        rawTrainingData.forEach(i => {
            trainingData.push({
                input: i[0],
                output: i[1]
            })
        })

        return trainingData
    }

    run(input) {
        return this.net.run(input)
    }

    get json() {
        return this.json
    }
}

module.exports = NNProcessor